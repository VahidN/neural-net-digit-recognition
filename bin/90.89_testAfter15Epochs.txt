Starting Epoch #0. Using learning rate: 0,06375
Average error: 1,2786
Starting Epoch #1. Using learning rate: 0,0541875
Average error: 0,8162
Starting Epoch #2. Using learning rate: 0,046059375
Average error: 0,4930
Starting Epoch #3. Using learning rate: 0,03915046875
Average error: 0,3750
Starting Epoch #4. Using learning rate: 0,0332778984375
Average error: 0,3453
Starting Epoch #5. Using learning rate: 0,028286213671875
Average error: 0,3271
Starting Epoch #6. Using learning rate: 0,0240432816210937
Average error: 0,3139
Starting Epoch #7. Using learning rate: 0,0204367893779297
Average error: 0,3041
Starting Epoch #8. Using learning rate: 0,0173712709712402
Average error: 0,2959
Starting Epoch #9. Using learning rate: 0,0147655803255542
Average error: 0,2893
Starting Epoch #10. Using learning rate: 0,0125507432767211
Average error: 0,2844
Starting Epoch #11. Using learning rate: 0,0106681317852129
Average error: 0,2800
Starting Epoch #12. Using learning rate: 0,00906791201743097
Average error: 0,2762
Starting Epoch #13. Using learning rate: 0,00770772521481632
Average error: 0,2730
Starting Epoch #14. Using learning rate: 0,00655156643259388
Average error: 0,2704
Test result: 90,89% of 10000 digits were recognized correctly
Incorrectly recognized (expected->actual = count):
4->9 = 67
9->4 = 62
5->3 = 44
7->9 = 37
8->3 = 32
8->5 = 31
2->3 = 31
5->8 = 28
3->5 = 27
3->8 = 26
2->8 = 21
7->2 = 20
9->7 = 19
3->7 = 19
6->4 = 19
8->4 = 19
2->0 = 18
9->8 = 17
3->2 = 16
2->6 = 15
2->5 = 14
7->4 = 14
6->0 = 12
8->9 = 12
4->6 = 11
2->7 = 11
5->6 = 11
0->4 = 11
8->0 = 11
5->0 = 10
5->7 = 10
5->4 = 10
6->2 = 9
9->3 = 9
6->5 = 8
5->9 = 8
1->2 = 8
9->0 = 8
7->3 = 7
2->4 = 7
8->2 = 7
0->5 = 7
3->6 = 6
2->1 = 6
7->1 = 6
8->7 = 6
0->3 = 6
1->8 = 6
1->3 = 5
3->9 = 5
0->8 = 5
4->2 = 4
7->8 = 4
6->8 = 4
1->6 = 4
8->6 = 4
0->7 = 4
8->1 = 4
0->6 = 3
6->3 = 3
2->9 = 3
0->9 = 3
7->0 = 3
1->9 = 3
9->1 = 3
5->1 = 3
3->4 = 3
9->5 = 2
1->4 = 2
6->1 = 2
5->2 = 2
0->2 = 2
4->0 = 2
3->0 = 2
4->8 = 2
7->5 = 1
9->6 = 1
1->5 = 1
9->2 = 1
4->3 = 1
4->1 = 1

