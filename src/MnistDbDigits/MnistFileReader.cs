using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace LBraz.MnistDbDigits
{
    public class MnistFileReader : IDisposable
    {
        private const int HEADERSIZE_LABELS = 8;
        private const int HEADERSIZE_IMAGES = 16;

        private readonly MnistContentType _contentType;
        private readonly BinaryReader _readerImages;
        private readonly BinaryReader _readerLabels;

        private readonly Int32 _labelFileMagicNumber;
        private readonly Int32 _labelFileNumberOfItems;

        private readonly Int32 _imageFileMagicNumber;
        private readonly Int32 _imageFileNumberOfImages;
        private readonly Int32 _imageFileNumberOfRows;
        private readonly Int32 _imageFileNumberOfColumns;
        private readonly Int32 _pixelCount;

        public MnistFileReader(MnistContentType contentType)
        {
            _contentType = contentType;

            string filePathImages = null;
            string filePathLabels = null;
            switch (_contentType)
            {
                case MnistContentType.TrainingSet:
                    filePathImages = "train-images.idx3-ubyte";
                    filePathLabels = "train-labels.idx1-ubyte";
                    break;
                case MnistContentType.TestSet:
                    filePathImages = "t10k-images.idx3-ubyte";
                    filePathLabels = "t10k-labels.idx1-ubyte";
                    break;
                default:
                    Debug.Fail("Unknown ContentType: " + _contentType);
                    break;
            }

            string assemblyLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            filePathImages = Path.Combine(assemblyLocation, filePathImages);
            filePathLabels = Path.Combine(assemblyLocation, filePathLabels);

            _readerImages = new BinaryReader(File.OpenRead(filePathImages), Encoding.BigEndianUnicode);
            _readerLabels = new BinaryReader(File.OpenRead(filePathLabels), Encoding.BigEndianUnicode);

            _labelFileMagicNumber       = ReadBigEndianInt32(_readerLabels);
            _labelFileNumberOfItems     = ReadBigEndianInt32(_readerLabels);

            _imageFileMagicNumber       = ReadBigEndianInt32(_readerImages);
            _imageFileNumberOfImages    = ReadBigEndianInt32(_readerImages);
            _imageFileNumberOfRows      = ReadBigEndianInt32(_readerImages);
            _imageFileNumberOfColumns   = ReadBigEndianInt32(_readerImages);
            _pixelCount = _imageFileNumberOfColumns * _imageFileNumberOfRows;

            Debug.Assert(_labelFileNumberOfItems == _imageFileNumberOfImages);

            Debug.Assert(HEADERSIZE_LABELS == _readerLabels.BaseStream.Position);
            Debug.Assert(HEADERSIZE_IMAGES == _readerImages.BaseStream.Position);
        }

        private static Int32 ReadBigEndianInt32(BinaryReader reader)
        {
            byte[] buffer = reader.ReadBytes(4);

			return buffer[0] << 24 |
                   buffer[1] << 16 |
                   buffer[2] << 8 |
                   buffer[3];
        }

        public int RecordCount
        {
            get { return _labelFileNumberOfItems; }
        }

        public MnistRecord ReadRecord(int position)
        {
            _readerLabels.BaseStream.Position = HEADERSIZE_LABELS + position;
            _readerImages.BaseStream.Position = HEADERSIZE_IMAGES + _pixelCount * position;

            byte label = _readerLabels.ReadByte();

            return new MnistRecord(label,
                _readerImages.ReadBytes(_imageFileNumberOfColumns * _imageFileNumberOfRows),
                _imageFileNumberOfColumns,
                _imageFileNumberOfRows);
        }

        #region IDisposable Members

        public void Dispose()
        {
            _readerImages.Close();
            _readerLabels.Close();
        }

        #endregion
    }
}
