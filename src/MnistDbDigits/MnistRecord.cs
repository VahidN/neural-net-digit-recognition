﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace LBraz.MnistDbDigits
{
    public class MnistRecord
    {
        public readonly byte Digit;
        public readonly int ImageWidth;
        public readonly int ImageHeight;
        public readonly byte[] ImageData;

        public MnistRecord(byte digit, byte[] image, int width, int height)
        {
            Digit = digit;
            ImageData = image;
            ImageWidth = width;
            ImageHeight = height;
        }

        public Image CreateImage()
        {
            Bitmap bmp = new Bitmap(ImageWidth, ImageHeight, PixelFormat.Format8bppIndexed);
            BitmapData data = bmp.LockBits(new Rectangle(new Point(), bmp.Size),
                ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            Marshal.Copy(ImageData, 0, data.Scan0, ImageData.Length);

            bmp.UnlockBits(data);

            return bmp;
        }
    }
}
