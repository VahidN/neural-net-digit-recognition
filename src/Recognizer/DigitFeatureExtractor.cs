using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;

namespace LBraz.Recognizer
{
    public class DigitFeatureExtractor
    {
        private readonly double[] _output;
        private readonly double[] _input;

        public const int InputsCount = 28 + 28 + 55 + 55; // for each histogram
        public const int OutputsCount = 10;

        private const int SLICE_COUNT = InputsCount / 4;

        public DigitFeatureExtractor(byte digit, ImageHistograms histograms)
        {
            _output = new double[OutputsCount];
            for (int i = 0; i < OutputsCount; ++i)
                _output[i] = -1;
            _output[digit] = 1;


            double[] horiz = histograms.Horizontal;
            double[] vert = histograms.Vertical;
            double[] diagLeft = histograms.LeftDiagonal;
            double[] diagRight = histograms.RightDiagonal;

            // Concatenate all histograms to one array.
            _input = new double[horiz.Length + vert.Length + diagLeft.Length + diagRight.Length];
            horiz.CopyTo(_input, 0);
            vert.CopyTo(_input, horiz.Length);
            diagLeft.CopyTo(_input, horiz.Length + vert.Length);
            diagRight.CopyTo(_input, horiz.Length + vert.Length + diagLeft.Length);
            Debug.Assert(_input.Length == InputsCount);
            
            // Scale all input values so that they would fit to range [-1;1] and their sum would be equal to 0.
            // This provides a better distribution of the inputs.
            double max = _input.Max();
            double average = _input.Average();
            for (int i = 0; i < _input.Length; ++i)
                _input[i] = (_input[i] - average) / max;
        }

        public double[] GetInput()
        {
            return _input;
        }

        public double[] GetOutput()
        {
            return _output;
        }
    }
}
