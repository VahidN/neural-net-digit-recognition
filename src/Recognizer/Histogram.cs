﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LBraz.Recognizer
{
    public partial class Histogram : UserControl
    {
        // color used to paing histogram
        private Color _color = Color.Black;
        // histogram's values
        private double[] _values;
        // max histogram's values
        private double _max;
        // vertical histogram or not
        private bool _vertical = false;
        // how much to expand each value's column
        private int _columnScale = 1;
        
        // set of pens
        private Pen _blackPen = new Pen(Color.Black, 1);
        private Pen _drawPen = new Pen(Color.Black);
        
        // width and height of histogram's area
        private int _width;
        private int _height;

        /// <summary>
        /// Histogram's color.
        /// </summary>
        [DefaultValue( typeof( Color ), "Black" )]
        public Color Color
        {
            get { return _color; }
            set
            {
                _color = value;

                _drawPen.Dispose( );
                _drawPen = new Pen( _color );
                Invalidate( );
            }
        }

        /// <summary>
        /// Vertical view or not.
        /// </summary>
        /// <remarks><para>The property determines if histogram should be displayed vertical or
        /// not (horizontally).</para>
        /// <para>By default the property is set to <b>false</b> - horizontal view.</para></remarks>
        [DefaultValue( false )]
        public bool Vertical
        {
            get { return _vertical; }
            set
            {
                _vertical = value;
                Invalidate();
            }
        }

        [Browsable(false)]
        public double MaxValue
        {
            get { return _max; }
            set
            {
                _max = value;
                Invalidate();
            }
        }

        [DefaultValue(1)]
        public int ColumnScale
        {
            get { return _columnScale; }
            set
            {
                _columnScale = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Histogram values.
        /// </summary>
        /// <remarks>Non-negative histogram values.</remarks>
        /// <exception cref="ArgumentException">Histogram has negative values.</exception>
        [Browsable(false)]
        public double[] Values
        {
            get { return _values; }
            set
            {
                _values = value;

                if ( _values != null )
                {
                    // check values and find maximum
                    _max = 0;
                    foreach (double v in _values)
                    {
                        // value chould non-negative
                        if ( v < 0 )
                        {
                            throw new ArgumentException( "Histogram values should be non-negative" );
                        }

                        if ( v > _max )
                        {
                            _max = v;
                        }
                    }
                }
                Invalidate( );
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Histogram"/> class.
        /// </summary>
        public Histogram()
        {
            InitializeComponent();
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.ResizeRedraw |
                ControlStyles.DoubleBuffer | ControlStyles.UserPaint, true);
        }

        /// <summary>
        /// Paint the control.
        /// </summary>
        /// <param name="pe">Data for Paint event.</param>
        protected override void OnPaint( PaintEventArgs pe )
        {
            Graphics g = pe.Graphics;
            // drawing area's width and height
            _width  = ( ( _values == null ) || ( _vertical == true ) ) ?
                ClientRectangle.Width - 2 :
                Math.Min( _values.Length, ClientRectangle.Width - 2 );

            _height = ( ( _values == null ) || ( _vertical == false ) ) ?
                ClientRectangle.Height - 2 :
                Math.Min(_values.Length, ClientRectangle.Height - 2);

            int x = 1;
            int y = 1;
            int value;

            if (_values != null)
            {
                if (_vertical)
                    _height *= _columnScale;
                else
                    _width *= _columnScale;
            }

            // draw rectangle around the image
            g.DrawRectangle(_blackPen, x - 1, y - 1, _width + 1, _height + 1);

            if (_values != null)
            {
                // scaling factor
                double factor = (_vertical ? _width : _height) / _max;

                // draw histogram
                for (int i = 0, len = _vertical ? _height : _width; i < len; i++)
                {
                    value = (int)(_values[i / _columnScale] * factor);

                    if (value != 0)
                    {
                        if (_vertical)
                        {
                            g.DrawLine(_drawPen,
                                new Point(x, y + i),
                                new Point(x + value, y + i)
                                );
                        }
                        else
                        {
                            g.DrawLine(_drawPen,
                                new Point(x + i, y + _height - 1),
                                new Point(x + i, y + _height - value)
                                );
                        }
                    }
                }
            }

            base.OnPaint(pe);
        }
    }
}
