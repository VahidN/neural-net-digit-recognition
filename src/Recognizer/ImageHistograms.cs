﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LBraz.Recognizer
{
    public class ImageHistograms
    {
        private readonly int _width;
        private readonly byte[] _image;

        public ImageHistograms(byte[] grayscaleImage, int width, int height)
        {
            if (width != height)
                throw new ArgumentException("Image must have same width and height.");
            _image = grayscaleImage;
            _width = width;
        }

        public double[] Horizontal
        {
            get
            {
                double[] ret = new double[_width];

                for (int y = 0; y < _width; ++y)
                {
                    for (int x = 0; x < _width; ++x)
                    {
                        ret[y] += _image[y * _width + x];
                    }
                }

                return ret;
            }
        }

        public double[] Vertical
        {
            get
            {
                double[] ret = new double[_width];

                for (int x = 0; x < _width; ++x)
                {
                    for (int y = 0; y < _width; ++y)
                    {
                        ret[x] += _image[y * _width + x];
                    }
                }

                return ret;
            }
        }

        public double[] RightDiagonal
        {
            get
            {
                // e.g.: 4x4 image:
                //
                // 0;0
                // 1;0 0;1
                // 2;0 1;1 0;2
                // 3;0 2;1 1;2 0;3
                // 3;1 2;2 1;3
                // 3;2 2;3
                // 3;3

                double[] ret = new double[_width * 2 - 1];

                for (int i = 0; i < _width; ++i)
                {
                    int pixelCount = i + 1;
                    for (int j = 0; j < pixelCount; ++j)
                    {
                        ret[i] += _image[(i - j) * _width + j];
                    }
                }
                for (int i = _width; i < ret.Length; ++i)
                {
                    int pixelCount = ret.Length - i;
                    for (int j = 1; j <= pixelCount; ++j)
                    {
                        ret[i] += _image[(_width - j) * _width + j + i - _width];
                    }
                }

                return ret;
            }
        }
        public double[] LeftDiagonal
        {
            get
            {
                // e.g.: 4x4 image:
                //
                // 3;0
                // 3;1 2;0
                // 3;2 2;1 1;0
                // 3;3 2;2 1;1 0;0
                // 2;3 1;2 0;1
                // 1;3 0;2
                // 0;3

                double[] ret = new double[_width * 2 - 1];

                for (int i = 0; i < _width; ++i)
                {
                    int pixelCount = i + 1;
                    for (int j = 0; j < pixelCount; ++j)
                    {
                        ret[i] += _image[(_width - 1 - j) * _width + i - j];
                    }
                }
                for (int i = _width; i < ret.Length; ++i)
                {
                    int pixelCount = ret.Length - i;
                    for (int j = 1; j <= pixelCount; ++j)
                    {
                        ret[i] += _image[(pixelCount - j) * _width + _width - j];
                    }
                }

                return ret;
            }
        }
    }
}
