namespace LBraz.Recognizer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem(new string[] {
            "0",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem(new string[] {
            "1",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem(new string[] {
            "2",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem(new string[] {
            "3",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem15 = new System.Windows.Forms.ListViewItem(new string[] {
            "4",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem16 = new System.Windows.Forms.ListViewItem(new string[] {
            "5",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem17 = new System.Windows.Forms.ListViewItem(new string[] {
            "6",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem18 = new System.Windows.Forms.ListViewItem(new string[] {
            "7",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem19 = new System.Windows.Forms.ListViewItem(new string[] {
            "8",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem20 = new System.Windows.Forms.ListViewItem(new string[] {
            "9",
            ""}, -1);
            this.backgroundWorkerTraining = new System.ComponentModel.BackgroundWorker();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSave = new System.Windows.Forms.ToolStripButton();
            this.tabPageTrainingTest = new System.Windows.Forms.TabPage();
            this.buttonStartTestOnTraining = new System.Windows.Forms.Button();
            this.buttonStartTestOnTest = new System.Windows.Forms.Button();
            this.progressBarTrainingAndTest = new System.Windows.Forms.ProgressBar();
            this.buttonClearLog = new System.Windows.Forms.Button();
            this.labelLog = new System.Windows.Forms.Label();
            this.buttonStopTrainingOrTest = new System.Windows.Forms.Button();
            this.buttonStartTraining = new System.Windows.Forms.Button();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.tabPageHistograms = new System.Windows.Forms.TabPage();
            this.histogramVert = new LBraz.Recognizer.Histogram();
            this.histogramHoriz = new LBraz.Recognizer.Histogram();
            this.labelInMax = new System.Windows.Forms.Label();
            this.labelOutMinus1 = new System.Windows.Forms.Label();
            this.labelHistogramHoriz = new System.Windows.Forms.Label();
            this.labelOutPlus1 = new System.Windows.Forms.Label();
            this.labelHistogramVert = new System.Windows.Forms.Label();
            this.checkBoxShowOnlyFailed = new System.Windows.Forms.CheckBox();
            this.labelANNResults = new System.Windows.Forms.Label();
            this.listViewRecognizedDigits = new System.Windows.Forms.ListView();
            this.Digit = new System.Windows.Forms.ColumnHeader();
            this.Output = new System.Windows.Forms.ColumnHeader();
            this.textBoxPicIndex = new System.Windows.Forms.TextBox();
            this.labelShowSelected = new System.Windows.Forms.Label();
            this.checkedListBoxDigits = new System.Windows.Forms.CheckedListBox();
            this.groupBoxDigitPic = new System.Windows.Forms.GroupBox();
            this.labelModif = new System.Windows.Forms.Label();
            this.labelOrig = new System.Windows.Forms.Label();
            this.pictureBoxProcessed = new System.Windows.Forms.PictureBox();
            this.pictureBoxDigit = new System.Windows.Forms.PictureBox();
            this.labelDigit = new System.Windows.Forms.Label();
            this.groupBoxFileType = new System.Windows.Forms.GroupBox();
            this.radioButtonTest = new System.Windows.Forms.RadioButton();
            this.radioButtonTrain = new System.Windows.Forms.RadioButton();
            this.buttonPrev = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.labelInMin = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonSettings = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.tabPageTrainingTest.SuspendLayout();
            this.tabPageHistograms.SuspendLayout();
            this.groupBoxDigitPic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProcessed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDigit)).BeginInit();
            this.groupBoxFileType.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorkerTraining
            // 
            this.backgroundWorkerTraining.WorkerReportsProgress = true;
            this.backgroundWorkerTraining.WorkerSupportsCancellation = true;
            this.backgroundWorkerTraining.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackgroundWorkerTraining_DoWork);
            this.backgroundWorkerTraining.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BackgroundWorkerTraining_RunWorkerCompleted);
            this.backgroundWorkerTraining.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BackgroundWorkerTraining_ProgressChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonNew,
            this.toolStripButtonOpen,
            this.toolStripButtonSave,
            this.toolStripSeparator1,
            this.toolStripButtonSettings});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(617, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonNew
            // 
            this.toolStripButtonNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonNew.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNew.Image")));
            this.toolStripButtonNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNew.Name = "toolStripButtonNew";
            this.toolStripButtonNew.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonNew.Text = "New neural network";
            this.toolStripButtonNew.Click += new System.EventHandler(this.toolStripButtonNew_Click);
            // 
            // toolStripButtonOpen
            // 
            this.toolStripButtonOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOpen.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOpen.Image")));
            this.toolStripButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpen.Name = "toolStripButtonOpen";
            this.toolStripButtonOpen.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonOpen.Text = "Open neural network";
            this.toolStripButtonOpen.Click += new System.EventHandler(this.toolStripButtonOpen_Click);
            // 
            // toolStripButtonSave
            // 
            this.toolStripButtonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSave.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSave.Image")));
            this.toolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSave.Name = "toolStripButtonSave";
            this.toolStripButtonSave.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSave.Text = "Save neural network";
            this.toolStripButtonSave.Click += new System.EventHandler(this.toolStripButtonSave_Click);
            // 
            // tabPageTrainingTest
            // 
            this.tabPageTrainingTest.Controls.Add(this.buttonStartTestOnTraining);
            this.tabPageTrainingTest.Controls.Add(this.buttonStartTestOnTest);
            this.tabPageTrainingTest.Controls.Add(this.progressBarTrainingAndTest);
            this.tabPageTrainingTest.Controls.Add(this.buttonClearLog);
            this.tabPageTrainingTest.Controls.Add(this.labelLog);
            this.tabPageTrainingTest.Controls.Add(this.buttonStopTrainingOrTest);
            this.tabPageTrainingTest.Controls.Add(this.buttonStartTraining);
            this.tabPageTrainingTest.Controls.Add(this.richTextBoxLog);
            this.tabPageTrainingTest.Location = new System.Drawing.Point(4, 22);
            this.tabPageTrainingTest.Name = "tabPageTrainingTest";
            this.tabPageTrainingTest.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTrainingTest.Size = new System.Drawing.Size(609, 393);
            this.tabPageTrainingTest.TabIndex = 1;
            this.tabPageTrainingTest.Text = "Training/Test";
            this.tabPageTrainingTest.UseVisualStyleBackColor = true;
            // 
            // buttonStartTestOnTraining
            // 
            this.buttonStartTestOnTraining.Location = new System.Drawing.Point(6, 64);
            this.buttonStartTestOnTraining.Name = "buttonStartTestOnTraining";
            this.buttonStartTestOnTraining.Size = new System.Drawing.Size(112, 23);
            this.buttonStartTestOnTraining.TabIndex = 2;
            this.buttonStartTestOnTraining.Text = "Test on training data";
            this.buttonStartTestOnTraining.UseVisualStyleBackColor = true;
            this.buttonStartTestOnTraining.Click += new System.EventHandler(this.ButtonStartTrainingOrTest_Click);
            // 
            // buttonStartTestOnTest
            // 
            this.buttonStartTestOnTest.Location = new System.Drawing.Point(6, 35);
            this.buttonStartTestOnTest.Name = "buttonStartTestOnTest";
            this.buttonStartTestOnTest.Size = new System.Drawing.Size(112, 23);
            this.buttonStartTestOnTest.TabIndex = 1;
            this.buttonStartTestOnTest.Text = "Test on test data";
            this.buttonStartTestOnTest.UseVisualStyleBackColor = true;
            this.buttonStartTestOnTest.Click += new System.EventHandler(this.ButtonStartTrainingOrTest_Click);
            // 
            // progressBarTrainingAndTest
            // 
            this.progressBarTrainingAndTest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarTrainingAndTest.Location = new System.Drawing.Point(168, 35);
            this.progressBarTrainingAndTest.Name = "progressBarTrainingAndTest";
            this.progressBarTrainingAndTest.Size = new System.Drawing.Size(435, 23);
            this.progressBarTrainingAndTest.TabIndex = 4;
            // 
            // buttonClearLog
            // 
            this.buttonClearLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClearLog.Location = new System.Drawing.Point(562, 83);
            this.buttonClearLog.Name = "buttonClearLog";
            this.buttonClearLog.Size = new System.Drawing.Size(41, 20);
            this.buttonClearLog.TabIndex = 6;
            this.buttonClearLog.Text = "Clear";
            this.buttonClearLog.UseVisualStyleBackColor = true;
            this.buttonClearLog.Click += new System.EventHandler(this.ButtonClearLog_Click);
            // 
            // labelLog
            // 
            this.labelLog.AutoSize = true;
            this.labelLog.Location = new System.Drawing.Point(8, 90);
            this.labelLog.Name = "labelLog";
            this.labelLog.Size = new System.Drawing.Size(28, 13);
            this.labelLog.TabIndex = 5;
            this.labelLog.Text = "Log:";
            // 
            // buttonStopTrainingOrTest
            // 
            this.buttonStopTrainingOrTest.Enabled = false;
            this.buttonStopTrainingOrTest.Location = new System.Drawing.Point(124, 35);
            this.buttonStopTrainingOrTest.Name = "buttonStopTrainingOrTest";
            this.buttonStopTrainingOrTest.Size = new System.Drawing.Size(38, 23);
            this.buttonStopTrainingOrTest.TabIndex = 3;
            this.buttonStopTrainingOrTest.Text = "Stop";
            this.buttonStopTrainingOrTest.UseVisualStyleBackColor = true;
            this.buttonStopTrainingOrTest.Click += new System.EventHandler(this.ButtonStopTrainingOrTest_Click);
            // 
            // buttonStartTraining
            // 
            this.buttonStartTraining.Location = new System.Drawing.Point(6, 6);
            this.buttonStartTraining.Name = "buttonStartTraining";
            this.buttonStartTraining.Size = new System.Drawing.Size(112, 23);
            this.buttonStartTraining.TabIndex = 0;
            this.buttonStartTraining.Text = "Start training";
            this.buttonStartTraining.UseVisualStyleBackColor = true;
            this.buttonStartTraining.Click += new System.EventHandler(this.ButtonStartTrainingOrTest_Click);
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxLog.Location = new System.Drawing.Point(6, 104);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.ReadOnly = true;
            this.richTextBoxLog.Size = new System.Drawing.Size(597, 281);
            this.richTextBoxLog.TabIndex = 7;
            this.richTextBoxLog.Text = "";
            // 
            // tabPageHistograms
            // 
            this.tabPageHistograms.Controls.Add(this.histogramVert);
            this.tabPageHistograms.Controls.Add(this.histogramHoriz);
            this.tabPageHistograms.Controls.Add(this.labelInMax);
            this.tabPageHistograms.Controls.Add(this.labelOutMinus1);
            this.tabPageHistograms.Controls.Add(this.labelHistogramHoriz);
            this.tabPageHistograms.Controls.Add(this.labelOutPlus1);
            this.tabPageHistograms.Controls.Add(this.labelHistogramVert);
            this.tabPageHistograms.Controls.Add(this.checkBoxShowOnlyFailed);
            this.tabPageHistograms.Controls.Add(this.labelANNResults);
            this.tabPageHistograms.Controls.Add(this.listViewRecognizedDigits);
            this.tabPageHistograms.Controls.Add(this.textBoxPicIndex);
            this.tabPageHistograms.Controls.Add(this.labelShowSelected);
            this.tabPageHistograms.Controls.Add(this.checkedListBoxDigits);
            this.tabPageHistograms.Controls.Add(this.groupBoxDigitPic);
            this.tabPageHistograms.Controls.Add(this.groupBoxFileType);
            this.tabPageHistograms.Controls.Add(this.buttonPrev);
            this.tabPageHistograms.Controls.Add(this.buttonNext);
            this.tabPageHistograms.Controls.Add(this.labelInMin);
            this.tabPageHistograms.Location = new System.Drawing.Point(4, 22);
            this.tabPageHistograms.Name = "tabPageHistograms";
            this.tabPageHistograms.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHistograms.Size = new System.Drawing.Size(609, 393);
            this.tabPageHistograms.TabIndex = 0;
            this.tabPageHistograms.Tag = "";
            this.tabPageHistograms.Text = "Digits preview";
            this.tabPageHistograms.UseVisualStyleBackColor = true;
            // 
            // histogramVert
            // 
            this.histogramVert.Color = System.Drawing.Color.Green;
            this.histogramVert.ColumnScale = 2;
            this.histogramVert.Location = new System.Drawing.Point(399, 164);
            this.histogramVert.MaxValue = 0;
            this.histogramVert.Name = "histogramVert";
            this.histogramVert.Size = new System.Drawing.Size(37, 193);
            this.histogramVert.TabIndex = 17;
            this.histogramVert.Values = null;
            // 
            // histogramHoriz
            // 
            this.histogramHoriz.Color = System.Drawing.Color.Green;
            this.histogramHoriz.ColumnScale = 2;
            this.histogramHoriz.Location = new System.Drawing.Point(27, 164);
            this.histogramHoriz.MaxValue = 0;
            this.histogramHoriz.Name = "histogramHoriz";
            this.histogramHoriz.Size = new System.Drawing.Size(357, 193);
            this.histogramHoriz.TabIndex = 13;
            this.histogramHoriz.Values = null;
            // 
            // labelInMax
            // 
            this.labelInMax.Location = new System.Drawing.Point(-4, 159);
            this.labelInMax.Name = "labelInMax";
            this.labelInMax.Size = new System.Drawing.Size(34, 13);
            this.labelInMax.TabIndex = 11;
            this.labelInMax.Text = "0";
            this.labelInMax.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelOutMinus1
            // 
            this.labelOutMinus1.AutoSize = true;
            this.labelOutMinus1.Location = new System.Drawing.Point(387, 348);
            this.labelOutMinus1.Name = "labelOutMinus1";
            this.labelOutMinus1.Size = new System.Drawing.Size(16, 13);
            this.labelOutMinus1.TabIndex = 16;
            this.labelOutMinus1.Text = "-1";
            // 
            // labelHistogramHoriz
            // 
            this.labelHistogramHoriz.AutoSize = true;
            this.labelHistogramHoriz.Location = new System.Drawing.Point(24, 145);
            this.labelHistogramHoriz.Margin = new System.Windows.Forms.Padding(3);
            this.labelHistogramHoriz.Name = "labelHistogramHoriz";
            this.labelHistogramHoriz.Size = new System.Drawing.Size(275, 13);
            this.labelHistogramHoriz.TabIndex = 10;
            this.labelHistogramHoriz.Text = "Input (Horizontal | Vertical | Left diagonal | Right diagonal)";
            // 
            // labelOutPlus1
            // 
            this.labelOutPlus1.AutoSize = true;
            this.labelOutPlus1.Location = new System.Drawing.Point(390, 159);
            this.labelOutPlus1.Name = "labelOutPlus1";
            this.labelOutPlus1.Size = new System.Drawing.Size(13, 13);
            this.labelOutPlus1.TabIndex = 15;
            this.labelOutPlus1.Text = "1";
            // 
            // labelHistogramVert
            // 
            this.labelHistogramVert.AutoSize = true;
            this.labelHistogramVert.Location = new System.Drawing.Point(396, 145);
            this.labelHistogramVert.Margin = new System.Windows.Forms.Padding(3);
            this.labelHistogramVert.Name = "labelHistogramVert";
            this.labelHistogramVert.Size = new System.Drawing.Size(63, 13);
            this.labelHistogramVert.TabIndex = 14;
            this.labelHistogramVert.Text = "Output (0-9)";
            // 
            // checkBoxShowOnlyFailed
            // 
            this.checkBoxShowOnlyFailed.AutoSize = true;
            this.checkBoxShowOnlyFailed.Location = new System.Drawing.Point(188, 105);
            this.checkBoxShowOnlyFailed.Name = "checkBoxShowOnlyFailed";
            this.checkBoxShowOnlyFailed.Size = new System.Drawing.Size(224, 17);
            this.checkBoxShowOnlyFailed.TabIndex = 7;
            this.checkBoxShowOnlyFailed.Text = "Show only digits unrecognized by network";
            this.checkBoxShowOnlyFailed.UseVisualStyleBackColor = true;
            // 
            // labelANNResults
            // 
            this.labelANNResults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelANNResults.Location = new System.Drawing.Point(418, 6);
            this.labelANNResults.Name = "labelANNResults";
            this.labelANNResults.Size = new System.Drawing.Size(53, 71);
            this.labelANNResults.TabIndex = 8;
            this.labelANNResults.Text = "Current network result on current image:";
            // 
            // listViewRecognizedDigits
            // 
            this.listViewRecognizedDigits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewRecognizedDigits.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Digit,
            this.Output});
            this.listViewRecognizedDigits.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14,
            listViewItem15,
            listViewItem16,
            listViewItem17,
            listViewItem18,
            listViewItem19,
            listViewItem20});
            this.listViewRecognizedDigits.Location = new System.Drawing.Point(477, 6);
            this.listViewRecognizedDigits.Name = "listViewRecognizedDigits";
            this.listViewRecognizedDigits.Size = new System.Drawing.Size(124, 230);
            this.listViewRecognizedDigits.TabIndex = 9;
            this.listViewRecognizedDigits.UseCompatibleStateImageBehavior = false;
            this.listViewRecognizedDigits.View = System.Windows.Forms.View.Details;
            // 
            // Digit
            // 
            this.Digit.Text = "Digit";
            this.Digit.Width = 36;
            // 
            // Output
            // 
            this.Output.Text = "Output";
            this.Output.Width = 84;
            // 
            // textBoxPicIndex
            // 
            this.textBoxPicIndex.Location = new System.Drawing.Point(37, 8);
            this.textBoxPicIndex.Name = "textBoxPicIndex";
            this.textBoxPicIndex.Size = new System.Drawing.Size(91, 20);
            this.textBoxPicIndex.TabIndex = 1;
            this.textBoxPicIndex.Text = "0";
            this.textBoxPicIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxPicIndex.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBoxPicIndex_KeyUp);
            // 
            // labelShowSelected
            // 
            this.labelShowSelected.AutoSize = true;
            this.labelShowSelected.Location = new System.Drawing.Point(185, 3);
            this.labelShowSelected.Name = "labelShowSelected";
            this.labelShowSelected.Size = new System.Drawing.Size(129, 13);
            this.labelShowSelected.TabIndex = 5;
            this.labelShowSelected.Text = "Show only selected digits:";
            // 
            // checkedListBoxDigits
            // 
            this.checkedListBoxDigits.CheckOnClick = true;
            this.checkedListBoxDigits.ColumnWidth = 40;
            this.checkedListBoxDigits.FormattingEnabled = true;
            this.checkedListBoxDigits.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.checkedListBoxDigits.Location = new System.Drawing.Point(206, 19);
            this.checkedListBoxDigits.MultiColumn = true;
            this.checkedListBoxDigits.Name = "checkedListBoxDigits";
            this.checkedListBoxDigits.Size = new System.Drawing.Size(93, 79);
            this.checkedListBoxDigits.TabIndex = 6;
            // 
            // groupBoxDigitPic
            // 
            this.groupBoxDigitPic.Controls.Add(this.labelModif);
            this.groupBoxDigitPic.Controls.Add(this.labelOrig);
            this.groupBoxDigitPic.Controls.Add(this.pictureBoxProcessed);
            this.groupBoxDigitPic.Controls.Add(this.pictureBoxDigit);
            this.groupBoxDigitPic.Controls.Add(this.labelDigit);
            this.groupBoxDigitPic.Location = new System.Drawing.Point(88, 34);
            this.groupBoxDigitPic.Name = "groupBoxDigitPic";
            this.groupBoxDigitPic.Size = new System.Drawing.Size(94, 64);
            this.groupBoxDigitPic.TabIndex = 4;
            this.groupBoxDigitPic.TabStop = false;
            this.groupBoxDigitPic.Text = "Digit";
            // 
            // labelModif
            // 
            this.labelModif.AutoSize = true;
            this.labelModif.Location = new System.Drawing.Point(47, 48);
            this.labelModif.Name = "labelModif";
            this.labelModif.Size = new System.Drawing.Size(47, 13);
            this.labelModif.TabIndex = 2;
            this.labelModif.Text = "Modified";
            // 
            // labelOrig
            // 
            this.labelOrig.AutoSize = true;
            this.labelOrig.Location = new System.Drawing.Point(3, 15);
            this.labelOrig.Name = "labelOrig";
            this.labelOrig.Size = new System.Drawing.Size(42, 13);
            this.labelOrig.TabIndex = 0;
            this.labelOrig.Text = "Original";
            // 
            // pictureBoxProcessed
            // 
            this.pictureBoxProcessed.Location = new System.Drawing.Point(59, 19);
            this.pictureBoxProcessed.Name = "pictureBoxProcessed";
            this.pictureBoxProcessed.Size = new System.Drawing.Size(28, 28);
            this.pictureBoxProcessed.TabIndex = 9;
            this.pictureBoxProcessed.TabStop = false;
            // 
            // pictureBoxDigit
            // 
            this.pictureBoxDigit.Location = new System.Drawing.Point(6, 30);
            this.pictureBoxDigit.Name = "pictureBoxDigit";
            this.pictureBoxDigit.Size = new System.Drawing.Size(28, 28);
            this.pictureBoxDigit.TabIndex = 6;
            this.pictureBoxDigit.TabStop = false;
            // 
            // labelDigit
            // 
            this.labelDigit.AutoSize = true;
            this.labelDigit.Location = new System.Drawing.Point(40, 30);
            this.labelDigit.Name = "labelDigit";
            this.labelDigit.Size = new System.Drawing.Size(13, 13);
            this.labelDigit.TabIndex = 1;
            this.labelDigit.Tag = "";
            this.labelDigit.Text = "?";
            // 
            // groupBoxFileType
            // 
            this.groupBoxFileType.Controls.Add(this.radioButtonTest);
            this.groupBoxFileType.Controls.Add(this.radioButtonTrain);
            this.groupBoxFileType.Location = new System.Drawing.Point(8, 34);
            this.groupBoxFileType.Name = "groupBoxFileType";
            this.groupBoxFileType.Size = new System.Drawing.Size(74, 64);
            this.groupBoxFileType.TabIndex = 3;
            this.groupBoxFileType.TabStop = false;
            this.groupBoxFileType.Text = "Data set";
            // 
            // radioButtonTest
            // 
            this.radioButtonTest.AutoSize = true;
            this.radioButtonTest.Location = new System.Drawing.Point(6, 42);
            this.radioButtonTest.Name = "radioButtonTest";
            this.radioButtonTest.Size = new System.Drawing.Size(46, 17);
            this.radioButtonTest.TabIndex = 1;
            this.radioButtonTest.Text = "Test";
            this.radioButtonTest.UseVisualStyleBackColor = true;
            this.radioButtonTest.CheckedChanged += new System.EventHandler(this.RadioButtonTrainOrTest_CheckedChanged);
            // 
            // radioButtonTrain
            // 
            this.radioButtonTrain.AutoSize = true;
            this.radioButtonTrain.Checked = true;
            this.radioButtonTrain.Location = new System.Drawing.Point(6, 19);
            this.radioButtonTrain.Name = "radioButtonTrain";
            this.radioButtonTrain.Size = new System.Drawing.Size(63, 17);
            this.radioButtonTrain.TabIndex = 0;
            this.radioButtonTrain.TabStop = true;
            this.radioButtonTrain.Text = "Training";
            this.radioButtonTrain.UseVisualStyleBackColor = true;
            // 
            // buttonPrev
            // 
            this.buttonPrev.Location = new System.Drawing.Point(8, 6);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(23, 23);
            this.buttonPrev.TabIndex = 0;
            this.buttonPrev.Text = "<";
            this.buttonPrev.UseVisualStyleBackColor = true;
            this.buttonPrev.Click += new System.EventHandler(this.ButtonPrevOrNext_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.Location = new System.Drawing.Point(134, 6);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(23, 23);
            this.buttonNext.TabIndex = 2;
            this.buttonNext.Text = ">";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.ButtonPrevOrNext_Click);
            // 
            // labelInMin
            // 
            this.labelInMin.Location = new System.Drawing.Point(-4, 348);
            this.labelInMin.Name = "labelInMin";
            this.labelInMin.Size = new System.Drawing.Size(34, 13);
            this.labelInMin.TabIndex = 12;
            this.labelInMin.Text = "0";
            this.labelInMin.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageHistograms);
            this.tabControl1.Controls.Add(this.tabPageTrainingTest);
            this.tabControl1.Location = new System.Drawing.Point(0, 28);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(617, 419);
            this.tabControl1.TabIndex = 1;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonSettings
            // 
            this.toolStripButtonSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSettings.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSettings.Image")));
            this.toolStripButtonSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSettings.Name = "toolStripButtonSettings";
            this.toolStripButtonSettings.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSettings.Text = "Neural network settings";
            this.toolStripButtonSettings.Click += new System.EventHandler(this.toolStripButtonSettings_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 447);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainForm";
            this.Text = "Digit recognition neural net (On MNIST DB)";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabPageTrainingTest.ResumeLayout(false);
            this.tabPageTrainingTest.PerformLayout();
            this.tabPageHistograms.ResumeLayout(false);
            this.tabPageHistograms.PerformLayout();
            this.groupBoxDigitPic.ResumeLayout(false);
            this.groupBoxDigitPic.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProcessed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDigit)).EndInit();
            this.groupBoxFileType.ResumeLayout(false);
            this.groupBoxFileType.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorkerTraining;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonOpen;
        private System.Windows.Forms.ToolStripButton toolStripButtonSave;
        private System.Windows.Forms.TabPage tabPageTrainingTest;
        private System.Windows.Forms.Button buttonStartTestOnTest;
        private System.Windows.Forms.ProgressBar progressBarTrainingAndTest;
        private System.Windows.Forms.Button buttonClearLog;
        private System.Windows.Forms.Label labelLog;
        private System.Windows.Forms.Button buttonStopTrainingOrTest;
        private System.Windows.Forms.Button buttonStartTraining;
        private System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.TabPage tabPageHistograms;
        private System.Windows.Forms.Label labelANNResults;
        private System.Windows.Forms.ListView listViewRecognizedDigits;
        private System.Windows.Forms.ColumnHeader Digit;
        private System.Windows.Forms.ColumnHeader Output;
        private System.Windows.Forms.TextBox textBoxPicIndex;
        private System.Windows.Forms.Label labelShowSelected;
        private System.Windows.Forms.CheckedListBox checkedListBoxDigits;
        private System.Windows.Forms.GroupBox groupBoxDigitPic;
        private System.Windows.Forms.Label labelModif;
        private System.Windows.Forms.Label labelOrig;
        private System.Windows.Forms.PictureBox pictureBoxProcessed;
        private System.Windows.Forms.PictureBox pictureBoxDigit;
        private System.Windows.Forms.Label labelDigit;
        private System.Windows.Forms.GroupBox groupBoxFileType;
        private System.Windows.Forms.RadioButton radioButtonTest;
        private System.Windows.Forms.RadioButton radioButtonTrain;
        private Histogram histogramVert;
        private System.Windows.Forms.Label labelHistogramHoriz;
        private System.Windows.Forms.Label labelHistogramVert;
        private Histogram histogramHoriz;
        private System.Windows.Forms.Button buttonPrev;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.CheckBox checkBoxShowOnlyFailed;
        private System.Windows.Forms.Button buttonStartTestOnTraining;
        private System.Windows.Forms.ToolStripButton toolStripButtonNew;
        private System.Windows.Forms.Label labelOutPlus1;
        private System.Windows.Forms.Label labelOutMinus1;
        private System.Windows.Forms.Label labelInMax;
        private System.Windows.Forms.Label labelInMin;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonSettings;

    }
}

