using System;
using System.Linq;
using System.Windows.Forms;
using LBraz.MnistDbDigits;
using System.ComponentModel;
using AForge.Neuro;
using AForge.Neuro.Learning;
using System.Collections.Generic;
using AForge.Imaging.Filters;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.IO;
using System.Reflection;

namespace LBraz.Recognizer
{
    public partial class MainForm : Form
    {
        // how many digits to skip before reporting learning or testing progress.
        private const int REPORT_PROGRESS_EVERY_N_DIGIT = 100;

        private readonly string _originalFormText;

        private MnistFileReader _mnistTrainingReader;
        private MnistFileReader _mnistTestingReader;
        private ActivationNetwork _ann;
        private NeuralNetOptions _opts = new NeuralNetOptions();

        public MainForm()
        {
            InitializeComponent();
            _originalFormText = Text;

            for (int i = 0; i < 10; ++i)
                checkedListBoxDigits.SetItemChecked(i, true);

            LoadMnist();
            CreateAnn();
        }

        private void LoadMnist()
        {
            try
            {
            _mnistTrainingReader = new MnistFileReader(MnistContentType.TrainingSet);
            _mnistTestingReader = new MnistFileReader(MnistContentType.TestSet);
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not load MNIST. Reason:\n" + e.Message);
                Environment.Exit(1);
            }
        }

        private void CreateAnn()
        {
            _ann = new ActivationNetwork(
                new BipolarSigmoidFunction(),
                _opts.InputLayerSize,
                _opts.HiddenLayerSize,
                _opts.OutputLayerSize);
        }

        private bool IsTrainingSet
        {
            get
            {
                return radioButtonTrain.Checked;
            }
        }

        private MnistFileReader CurrentReader
        {
            get
            {
                return IsTrainingSet ? _mnistTrainingReader : _mnistTestingReader;
            }
        }

        private int? SelectedPicIndex
        {
            get
            {
                int index;
                if (int.TryParse(textBoxPicIndex.Text, out index))
                    return index;
                return null;
            }
            set
            {
                textBoxPicIndex.Text = value.Value.ToString();
            }
        }

        private void UpdatePic()
        {
            int? index = SelectedPicIndex;
            if (!index.HasValue ||
                index.Value < 0 || index.Value >= CurrentReader.RecordCount)
            {
                return;
            }

            MnistRecord rec = CurrentReader.ReadRecord(index.Value);
            SetPicState(rec);
        }

        private static Bitmap PreProcessImage(Image image)
        {
            FiltersSequence seq = new FiltersSequence(
                new Threshold(),
                new SimpleSkeletonization());
            return seq.Apply((Bitmap)image);
        }
        private static byte[] ImageToBytes(Bitmap bmp)
        {
            byte[] bytes = new byte[bmp.Width * bmp.Height];

            BitmapData bmpData = bmp.LockBits(new Rectangle(new Point(), bmp.Size),
                ImageLockMode.ReadOnly,
                PixelFormat.Format8bppIndexed);

            Marshal.Copy(bmpData.Scan0, bytes, 0, bytes.Length);

            bmp.UnlockBits(bmpData);

            return bytes;
        }

        private void SetPicState(MnistRecord rec)
        {
            labelDigit.Text = rec.Digit.ToString();

            Bitmap bmp = (Bitmap)rec.CreateImage();
            pictureBoxDigit.Image = new GrayscaleY().Apply(bmp);

            bmp = PreProcessImage(bmp);
            pictureBoxProcessed.Image = bmp;

            DigitFeatureExtractor featExtr = CreateFeatExtractor(rec);

            double inMin = featExtr.GetInput().Min();
            double inMax = featExtr.GetInput().Max();
            labelInMin.Text = string.Format("{0:0.00}", inMin);
            labelInMax.Text = string.Format("{0:0.00}", inMax);

            histogramHoriz.Values = featExtr.GetInput().Select(val => val - inMin).ToArray();

            histogramVert.Values = featExtr.GetOutput().Select(res => res + 1).ToArray();


            double[] output = _ann.Compute(featExtr.GetInput());
            // Store the output in the listView so to look it up when filtering only unrecognized digits.
            listViewRecognizedDigits.Tag = output;

            foreach (ListViewItem item in listViewRecognizedDigits.Items)
            {
                double outp = output[item.Index];
                // map [-1;1] to [255;0]
                int intensity = 255 - (int)((outp + 1) / 2 * 255);
                item.SubItems[1].Text = outp.ToString();
                if (rec.Digit == item.Index)
                    item.BackColor = Color.FromArgb(intensity, 255, intensity);
                else
                    item.BackColor = Color.FromArgb(255, intensity, intensity);
            }
        }

        private void TextBoxPicIndex_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                UpdatePic();
            }
        }

        private void ButtonPrevOrNext_Click(object sender, EventArgs e)
        {
            int? index = SelectedPicIndex;
            if (!index.HasValue)
                return;

            while (true) // if needed, loop until we find an unrecognized digit.
            {
                do
                {
                    bool nextClicked = (sender == buttonNext);
                    index = index.Value + (nextClicked ? 1 : -1);
                    if (index.Value < 0 || index.Value >= CurrentReader.RecordCount)
                        return;
                } while (!checkedListBoxDigits.CheckedIndices.Contains(CurrentReader.ReadRecord(index.Value).Digit));

                SelectedPicIndex = index;

                UpdatePic();

                if (!checkBoxShowOnlyFailed.Checked)
                    break;

                double[] output = listViewRecognizedDigits.Tag as double[];
                if (output == null)
                    break;

                // Get the index in the biggest value in output.
                int recognizedDigit = output.Select((val, idx) => new { val, idx })
                                            .OrderByDescending(valAndIdx => valAndIdx.val)
                                            .First().idx;
                if (this.labelDigit.Text != recognizedDigit.ToString())
                    break;
            }
            
        }

        private void RadioButtonTrainOrTest_CheckedChanged(object sender, EventArgs e)
        {
            UpdatePic();
        }

        private void ButtonStartTrainingOrTest_Click(object sender, EventArgs e)
        {
            buttonStartTraining.Enabled = false;
            buttonStartTestOnTest.Enabled = false;
            buttonStartTestOnTraining.Enabled = false;

            bool training = buttonStartTraining == sender;
            bool testOnTestData = buttonStartTestOnTest == sender;

            backgroundWorkerTraining.RunWorkerAsync(new object[] { training, testOnTestData });

            buttonStopTrainingOrTest.Enabled = true;
        }

        private void ButtonStopTrainingOrTest_Click(object sender, EventArgs e)
        {
            buttonStopTrainingOrTest.Enabled = false;
            backgroundWorkerTraining.CancelAsync();
        }

        private void ButtonClearLog_Click(object sender, EventArgs e)
        {
            richTextBoxLog.Clear();
        }

        private static DigitFeatureExtractor CreateFeatExtractor(MnistRecord rec)
        {
            Bitmap bmp = PreProcessImage(rec.CreateImage());
            byte[] imageData = ImageToBytes(bmp);
            ImageHistograms hist = new ImageHistograms(imageData, rec.ImageWidth, rec.ImageHeight);
            DigitFeatureExtractor extr = new DigitFeatureExtractor(rec.Digit, hist);
            return extr;
        }

        private void BackgroundWorkerTraining_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] args = (object[])e.Argument;
            bool isTraining = (bool)args[0];
            bool testOnTestData = (bool)args[1];

            if (isTraining)
            {
                DoTraining(_mnistTrainingReader);
            }
            else
            {
                DoTest(testOnTestData ? _mnistTestingReader : _mnistTrainingReader);
            }
        }

        private void DoTest(MnistFileReader reader)
        {
            int recCount = reader.RecordCount;
            int correctCount = 0;
            int totalCount = 0;

            Dictionary<string, int> errors = new Dictionary<string, int>();
            for (int i = 0; i < recCount; ++i)
            {
                if (backgroundWorkerTraining.CancellationPending)
                {
                    return;
                }

                MnistRecord rec = reader.ReadRecord(i);
                DigitFeatureExtractor extr = CreateFeatExtractor(rec);
                double[] output = _ann.Compute(extr.GetInput());

                int expectedDigit = Array.IndexOf(extr.GetOutput(), 1.0);
                int actualDigit = output.Select((res, index) => new { res, index }).
                                         OrderByDescending((res) => res.res).
                                         First().index;

                totalCount++;
                if (expectedDigit == actualDigit)
                {
                    correctCount++;
                }
                else
                {
                    string key = expectedDigit + "->" + actualDigit;
                    int errorCount = 0;
                    errors.TryGetValue(key, out errorCount);
                    errors[key] = ++errorCount;
                }

                if (i % REPORT_PROGRESS_EVERY_N_DIGIT == 0)
                {
                    double progress = 100.0 * i / recCount;
                    backgroundWorkerTraining.ReportProgress((int)progress, null);
                }
            }

            double successPercent = correctCount * 100.0 / totalCount;
            string message = "Test result: " + successPercent + "% of " + totalCount + " digits were recognized correctly\n";
            message += "Incorrectly recognized (expected->actual = count):\n";
            foreach (KeyValuePair<string, int> error in errors.OrderByDescending(error => error.Value))
                message += error.Key + " = " + error.Value + "\n";
            backgroundWorkerTraining.ReportProgress(100, message);
        }

        private void DoTraining(MnistFileReader reader)
        {
            int recCount = reader.RecordCount;
            int epochCount = _opts.TrainingEpochCount;
            DigitFeatureExtractor[] cachedFeatExtrs = new DigitFeatureExtractor[recCount];

            BackPropagationLearning teacher = new BackPropagationLearning(_ann);
            teacher.LearningRate = _opts.StartingLearningRate;
            teacher.Momentum = _opts.LearningMomentum;
            for (int epoch = 0; epoch < epochCount; ++epoch)
            {
                backgroundWorkerTraining.ReportProgress((int)(epoch * 100.0 / epochCount), "Starting Epoch #" + epoch + ". Using learning rate: " + teacher.LearningRate);
                double sumErr = 0;

                for (int i = 0; i < recCount; ++i)
                {
                    if (backgroundWorkerTraining.CancellationPending)
                    {
                        return;
                    }

                    if (cachedFeatExtrs[i] == null)
                    {
                        MnistRecord rec = reader.ReadRecord(i);
                        DigitFeatureExtractor extr = CreateFeatExtractor(rec);
                        cachedFeatExtrs[i] = extr;
                    }
                    double error = teacher.Run(cachedFeatExtrs[i].GetInput(), cachedFeatExtrs[i].GetOutput());
                    sumErr += error;

                    if (i % REPORT_PROGRESS_EVERY_N_DIGIT == 0)
                    {
                        double progress = (epoch + (double)i / recCount) * 100.0 / epochCount;
                        backgroundWorkerTraining.ReportProgress((int)progress, null);
                    }
                }

                double percent = (epoch + 1) * 100.0 / epochCount;
                backgroundWorkerTraining.ReportProgress((int)percent,
                    string.Format("Average error: {0:0.0000}", sumErr / recCount));

                teacher.LearningRate = teacher.LearningRate * _opts.LearningRateDecay;
            }
        }

        private void BackgroundWorkerTraining_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBarTrainingAndTest.Value = e.ProgressPercentage;
            string msg = (string)e.UserState;
            if (msg != null)
                richTextBoxLog.AppendText(msg + Environment.NewLine);
        }

        private void BackgroundWorkerTraining_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            buttonStartTraining.Enabled = true;
            buttonStartTestOnTest.Enabled = true;
            buttonStartTestOnTraining.Enabled = true;
            buttonStopTrainingOrTest.Enabled = false;
            if (e.Error != null)
                MessageBox.Show(e.Error.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #region neural network files saving and loading

        private void PrepareDialog(FileDialog dlg)
        {
            dlg.Filter = "Neural network files (*.nnet)|*.nnet";
            dlg.InitialDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        private void toolStripButtonNew_Click(object sender, EventArgs e)
        {
            CreateAnn();
            Text = _originalFormText;
        }
        private void toolStripButtonOpen_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    PrepareDialog(dlg);
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        _ann = (ActivationNetwork)Network.Load(dlg.FileName);
                        Text = dlg.FileName + " - " + _originalFormText;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error opening", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void toolStripButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (SaveFileDialog dlg = new SaveFileDialog())
                {
                    PrepareDialog(dlg);
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        _ann.Save(dlg.FileName);
                        Text = dlg.FileName + " - " + _originalFormText;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error saving", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        #endregion

        private void toolStripButtonSettings_Click(object sender, EventArgs e)
        {
            new NNSettingsForm(_opts).ShowDialog();
        }

    }
}
