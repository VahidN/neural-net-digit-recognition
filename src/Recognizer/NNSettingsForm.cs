﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LBraz.Recognizer
{
    public partial class NNSettingsForm : Form
    {
        public NNSettingsForm(NeuralNetOptions options)
        {
            InitializeComponent();
            propertyGrid1.SelectedObject = options;
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            propertyGrid1.SelectedObject = new NeuralNetOptions();
        }
    }
}
