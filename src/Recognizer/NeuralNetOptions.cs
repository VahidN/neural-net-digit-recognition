﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using AForge;

namespace LBraz.Recognizer
{
    public class NeuralNetOptions
    {
        public NeuralNetOptions()
        {
            HiddenLayerSize = 15;
            TrainingEpochCount = 15;
            StartingLearningRate = 0.075;
            LearningRateDecay = 0.85;
            LearningMomentum = 0;
        }

        [CategoryAttribute("Structure")]
        [DescriptionAttribute("Number of inputs to the neural network.")]
        public int InputLayerSize
        {
            get { return DigitFeatureExtractor.InputsCount; }
        }

        [CategoryAttribute("Structure")]
        [DescriptionAttribute("Number of output from the neural network. It is also the number of neurons in the last layer.")]
        public int OutputLayerSize
        {
            get { return DigitFeatureExtractor.OutputsCount; }
        }

        private int _hiddenCount;
        [CategoryAttribute("Structure")]
        [DescriptionAttribute("Number of neurons in the hidden layer of the neural network.")]
        public int HiddenLayerSize
        {
            get { return _hiddenCount; }
            set
            {
                if (value < 1)
                    throw new ArgumentException("Hidden layer must have at least 1 neuron.");
                _hiddenCount = value;
            }
        }

        private int _epochs;
        [CategoryAttribute("Training")]
        [DescriptionAttribute("How many epochs (iterations) to use for training the neural network.")]
        public int TrainingEpochCount
        {
            get { return _epochs; }
            set
            {
                if (value < 1)
                    throw new ArgumentException("At least 1 epoch is required for training.");
                _epochs = value;
            }
        }

        private double _startLearnRate;
        [CategoryAttribute("Training")]
        [DescriptionAttribute("With what learning rate to start the teaching.")]
        public double StartingLearningRate
        {
            get { return _startLearnRate; }
            set
            {
                if (value < 0 || value > 1)
                    throw new ArgumentException("Learning rate must be within range [0; 1].");
                _startLearnRate = value;
            }
        }

        private double _learnRateDecay;
        [CategoryAttribute("Training")]
        [DescriptionAttribute("How much to reduce the learning rate after training epoch (LearningRate = LearningRate * LearningRateDecay).")]
        public double LearningRateDecay
        {
            get { return _learnRateDecay; }
            set
            {
                if (value < 0 || value > 1)
                    throw new ArgumentException("Learning rate decay must be within range [0; 1].");
                _learnRateDecay = value;
            }
        }

        private double _momentum;
        [CategoryAttribute("Training")]
        [DescriptionAttribute("How much to use the last weights update.")]
        public double LearningMomentum
        {
            get { return _momentum; }
            set
            {
                if (value < 0 || value > 1)
                    throw new ArgumentException("Momentum must be within range [0; 1].");
                _momentum = value;
            }
        }
    }
}
